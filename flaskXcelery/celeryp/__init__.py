from flask import Flask
from celeryp.flask_celery import make_celery
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate



app = Flask(__name__)
# app.config['CELERY_BROKER_URL']='redis://127.0.0.1:6379',
# app.config['CELERY_RESULT_BACKEND']='db+sqlite:///db.sqlite3',
app.config.update(
    CELERY_BROKER_URL='redis://127.0.0.1:6379',
    CELERY_RESULT_BACKEND='db+sqlite:///db.sqlite3'
)
app.config['SQLALCHEMY_DATABASE_URI']='sqlite:///site1.db'
celery = make_celery(app)

db = SQLAlchemy(app)
migrate = Migrate(app, db)
from celeryp import routes