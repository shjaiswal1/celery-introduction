from celeryp import app
from random import choice
from celeryp.models import Results
from celeryp import db
from celeryp import make_celery

celery = make_celery(app)

@app.route('/process/<name>')
def process (name):
   reverse.delay(name)
   return 'I sent an async request!'

@app.route('/insertData')
def insertData():
  insert.delay()
  return 'this should not take much time to load'

@app.route('/insertData1')
def insertData1():
  return insert1()


@celery.task (name='app.reverse')
def reverse(string):
    return string[::-1]


@celery.task (name='app.insert')
def insert(): 
    for i in range(100000):
      data = ''.join(choice('ABCDE') for i in range(10))
      result = Results (data=data)
      
      db.session.add(result)
    db.session.commit()
    return 'Done!!!'


def insert1(): 
    for i in range(100000):
      data = ''.join(choice('ABCDE') for i in range(10))
      result = Results (data=data)
      
      db.session.add(result)
    db.session.commit()
    return 'Done!!!'