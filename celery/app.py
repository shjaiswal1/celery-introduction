from celery import Celery 
from time import sleep

app = Celery('tasks', broker='redis://127.0.0.1:6379', backend='db+sqlite:///db.sqlite3')

@app.task 
def reverse(text):
    sleep(60)
    return text[::-1]

